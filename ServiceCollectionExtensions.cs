﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using SerilogILogger = Serilog.ILogger;

#if NETSTANDARD1_3
using System.Linq;
#endif

namespace XploRe.Logging.Serilog
{

    /// <summary>
    ///     Extension methods for <see cref="IServiceCollection" /> to add Serilog to the logger pipeline.
    /// </summary>
    public static class ServiceCollectionExtensions
    {

        /// <summary>
        ///     Adds logging services to the specified <see cref="IServiceCollection" /> and registers the Serilog
        ///     logging provider using the static <see cref="Log" /> logger, which will be disposed with the logger
        ///     factory.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///     On .NET Standard 1.6 and lower, either no <see cref="ILoggerFactory" /> or an instantiated singleton
        ///     must be registered yet. Otherwise, it is not possible to add Serilog to the factory.
        ///     </para>
        ///     <para>
        ///     On .NET Standard 2.0 and newer, the logging builder is used to register the Serilog provider.
        ///     </para>  
        /// </remarks>
        /// <param name="services">This <see cref="IServiceCollection" /> to add services to.</param>
        /// <returns>This <see cref="IServiceCollection" /> instance.</returns>
        [NotNull]
        public static IServiceCollection AddSerilog([NotNull] this IServiceCollection services)
        {
            // ReSharper disable once IntroduceOptionalParameters.Global
            return AddSerilog(services, (SerilogILogger) null);
        }

        /// <summary>
        ///     Adds logging services to the specified <see cref="IServiceCollection" /> and registers the Serilog
        ///     logging provider using the provided logger.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///     On .NET Standard 1.6 and lower, either no <see cref="ILoggerFactory" /> or an instantiated singleton
        ///     must be registered yet. Otherwise, it is not possible to add Serilog to the factory.
        ///     </para>
        ///     <para>
        ///     On .NET Standard 2.0 and newer, the logging builder is used to register the Serilog provider.
        ///     </para>  
        /// </remarks>
        /// <param name="services">This <see cref="IServiceCollection" /> to add services to.</param>
        /// <param name="serilogLogger">
        ///     The <see cref="SerilogILogger" /> to use. If <c>null</c>, the static <see cref="Log" /> logger is used
        ///     instead, which will be disposed with the logger factory.
        /// </param>
        /// <returns>This <see cref="IServiceCollection" /> instance.</returns>
        [NotNull]
        public static IServiceCollection AddSerilog(
            [NotNull] this IServiceCollection services,
            [CanBeNull] SerilogILogger serilogLogger)
        {
            
#if NETSTANDARD1_3

            if (services == null) {
                throw new ArgumentNullException(nameof(services));
            }

            // Find an already registered logger factory, otherwise create a new default logger factory.
            var factoryServiceDescriptor = services.FirstOrDefault(s => s?.ServiceType == typeof(ILoggerFactory));
            ILoggerFactory loggerFactory;

            if (factoryServiceDescriptor != null) {
                if (factoryServiceDescriptor.ImplementationInstance == null) {
                    throw new InvalidOperationException(
                        "Cannot add Serilog provider:" +
                        " An ILoggerFactory has already been registered without an accessible instance."
                    );
                }

                loggerFactory = factoryServiceDescriptor.ImplementationInstance as ILoggerFactory;

                if (loggerFactory == null) {
                    throw new InvalidOperationException(
                        "Registered ILoggerFactory instance does not implement ILoggerFactory."
                    );
                }
            }
            else {
                loggerFactory = new LoggerFactory();

                // Register the manually created logger factory.
                services.AddSingleton(loggerFactory);
            }

            // Register Serilog provider.
            loggerFactory.AddSerilog(serilogLogger, dispose: true);

            // Add other loggin services, if not yet registered.
            services.AddLogging();

#else

            AddSerilog(services, serilogLogger, builder => { });

#endif

            return services;
        }

#if !NETSTANDARD1_3

        /// <summary>
        ///     Adds logging services to the specified <see cref="IServiceCollection" /> and registers the Serilog
        ///     logging provider using the provided logger.
        /// </summary>
        /// <param name="services">This <see cref="IServiceCollection" /> to add services to.</param>
        /// <param name="serilogLogger">
        ///     The <see cref="SerilogILogger" /> to use. If <c>null</c>, the static <see cref="Log" /> logger is used
        ///     instead, which will be disposed with the logger factory.
        /// </param>
        /// <param name="configure">The <see cref="ILoggingBuilder" /> configuration delegate.</param>
        /// <returns>This <see cref="IServiceCollection" /> instance.</returns>
        [NotNull]
        public static IServiceCollection AddSerilog(
            [NotNull] this IServiceCollection services,
            [CanBeNull] SerilogILogger serilogLogger,
            [NotNull] Action<ILoggingBuilder> configure)
        {
            if (services == null) {
                throw new ArgumentNullException(nameof(services));
            }
            
            if (configure == null) {
                throw new ArgumentNullException(nameof(configure));
            }

            // Register Serilog provider in logger builder (API was introduced with .NET Core 2).
            services.AddLogging(
                builder => {
                    builder.AddSerilog(serilogLogger, dispose: true);
                    // By default, a minimum log level of Information is set. Reset to lowest log level to allow Serilog
                    // to filter messages itself.
                    builder.SetMinimumLevel(LogLevel.Trace);

                    // Let the user-supplied delegate overwrite default settings.
                    configure(builder);
                });

            return services;
        }

#endif

        /// <summary>
        ///     Adds logging services to the specified <see cref="IServiceCollection" /> that use the static
        ///     <see cref="Log" /> Serilog logger which is configured via the provided delegate. The static Serilog
        ///     logger will be automatically disposed with the logger factory service.
        /// </summary>
        /// <param name="services">This <see cref="IServiceCollection" /> to add services to.</param>
        /// <param name="configure">The <see cref="LoggerConfiguration" /> configuration delegate.</param>
        /// <returns>This <see cref="IServiceCollection" /> instance.</returns>
        [NotNull]
        public static IServiceCollection AddSerilog(
            [NotNull] this IServiceCollection services,
            [NotNull] Action<LoggerConfiguration> configure)
        {
            // ReSharper disable once IntroduceOptionalParameters.Global
            return AddSerilog(services, global: true, configure: configure);
        }

        /// <summary>
        ///     Adds logging services to the specified <see cref="IServiceCollection" /> that use the Serilog logger
        ///     which is configured via the provided delegate. The Serilog logger will be automatically disposed with
        ///     the logger factory service.
        /// </summary>
        /// <param name="services">This <see cref="IServiceCollection" /> to add services to.</param>
        /// <param name="global">
        ///     If set to <c>true</c>, the configured logger will be assigned to the global static <see cref="Log" />.
        ///     Otherwise, the configured logger will only be used by the logging service pipeline.
        /// </param>
        /// <param name="configure">The <see cref="LoggerConfiguration" /> configuration delegate.</param>
        /// <returns>This <see cref="IServiceCollection" /> instance.</returns>
        [NotNull]
        public static IServiceCollection AddSerilog(
            [NotNull] this IServiceCollection services,
            bool global,
            [NotNull] Action<LoggerConfiguration> configure)
        {
            if (services == null) {
                throw new ArgumentNullException(nameof(services));
            }
            
            if (configure == null) {
                throw new ArgumentNullException(nameof(configure));
            }

            // Configure and instantiate Serilog logger.
            var configuration = new LoggerConfiguration();
            configure(configuration);

            var logger = configuration.CreateLogger();

            if (global) {
                // Assign to static Log for global access.
                Log.Logger = logger;

                // Reset to null, such that the static Log will be used by AddSerilog().
                logger = null;
            }

            return AddSerilog(services, logger);
        }

    }

}
